# CP Ex - Read This First

## Assumptions

| Assumption                                                                                                                                                                                                                                                                                                                                                     | Reasoning                                                                                                  |
|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|------------------------------------------------------------------------------------------------------------|
| Interval request looks into the past.<br/> That is to say, for a <br/>`/stats?interval=10` request we return the counter in the context of the past ten seconds                                                                                                                                                                                                | Doesn't really matter, we could have gone the other way and waited with the request.                       |
| There is a `MAX_INTERVAL`. We do not save data from before this period.<br/> The default value for this period has been set to 60 seconds, for the sake of easy testing. Ergo - after 60 seconds of no events, the count will be zero. You may not request data for intervals larger than 60 seconds. This can be changed via the `MAX_INTERVAL` env variable. | In a real world scenario we would have a retention policy, maybe offloading old data to some other system. |
| The `POST /events` body is a JSON format:<br/>```{"sentence": "foo bar"}```                                                                                                                                                                                                                                                                                    | I think it looks nicer.                                                                                    |
| Case instensitive keywords                                                                                                                                                                                                                                                                                                                                     | Easier this way `¯\_(ツ)_/¯`                                                                                  |

## Decisions

| Decision                            | Reasoning                                                                                                                                                                                           |
|-------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Use Redis to manage counters (ZADD) | Redis gives me easy sorting by time and the ability to eliminate old data by trimming the list past a certain point. Runs in memory, is fast, and allows for future optimizations and easy scaling. |
| Using Docker compose to deploy      | Allows deployment of service and redis together, easily, plus it's cool.                                                                                                                            |
| Cleanup Redis every minute          | We want ot remove data that is older than `MAX_INTERVAL` (configurable via env var), to do this we have a cleanup thread that wakes up every second (configurable in code) |

# Code Architecture

## Directory structure

```commandline
├── README.md
├── build
│   └── package
│       └── Dockerfile
├── cpex
│   ├── __init__.py
│   ├── app.py
│   ├── exceptions.py
│   └── keyword_counter.py
├── deploy
│   └── docker-compose
│       └── docker-compose.yml
├── requirements.txt
├── tests
│   ├── __init__.py
│   ├── test_api.py
└── └── test_keywordcounter.py
```
* `build` - contains build related files, such as the Dockerfile
* `deploy` - deployment related files
* `cpex` - contains the app itself. The app is split into handler and "domain", where our backend the redis. A very simple format, fitting the scope of the task. In a real world context we would probably abstract further, etc. 
* `tests` - you guessed it. tests. 

### Mermaid Diagram
```mermaid
graph LR;

    Client --> Handler

    subgraph App
        Handler --> Domain
    end
    Domain --> Redis
```

# API Doc

[Open API](./docs/openapi3.yaml)

# Deploying

```bash
docker compose  -f deploy/docker-compose/docker-compose.yml up --build
```
# Running

The service runs on port `8000`.

## Modified CLI commands
CLI commands that work with the change of input to JSON format. 

### Create event
```bash
curl --location 'http://localhost:8000/api/v1/events' \
--header 'Content-Type: application/json' \
--data '{
    "sentence": "Avanan is a leading Enterprise Solution for Cloud Email and Collaboration Security"
}'
```

### Get events
```bash
curl --location 'http://localhost:8000/api/v1/stats?interval=3'
```
