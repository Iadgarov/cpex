from fastapi import HTTPException


class _BaseError(HTTPException):
    def __init__(self, msg: str):
        self.detail = msg
        super().__init__(status_code=self.status_code, detail=self.detail)


class ServerError(_BaseError):
    status_code = 500


class UserError(_BaseError):
    status_code = 422
