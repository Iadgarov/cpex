import asyncio
import os
import random
import time

import aioredis

from cpex.exceptions import ServerError


class KeywordCounter:
    def __init__(self, keywords, max_interval, cleanup_period=1):
        self.max_interval = max_interval
        self.cleanup_period = cleanup_period
        self.redis_key = "keyword_counters"
        self.keywords = keywords
        self.redis_url = os.environ.get("REDIS_URL", default="redis://localhost:6379")
        self.redis_pool = None

    async def get_redis_pool(self):
        if not self.redis_pool:
            self.redis_pool = await aioredis.from_url(self.redis_url, encoding="utf-8")
        return self.redis_pool

    async def count_keywords(self, sentence):
        counts = {kw: sentence.lower().count(kw) for kw in self.keywords}

        # important to avoid ZADD antipattern - https://redis.com/redis-best-practices/time-series/sorted-set-time-series/
        counts["random_seed"] = random.random()

        await self.add_counts_to_redis(counts)
        return counts

    async def add_counts_to_redis(self, counts):
        timestamp = int(time.time())
        try:
            redis_pool = await self.get_redis_pool()
            async with redis_pool.client() as conn:
                await conn.zadd(self.redis_key, {str(counts): timestamp})
        except aioredis.RedisError:
            raise ServerError("Failed to reach backend, how embarrassing")

    async def get_counts_by_timestamp_range(self, start_timestamp, end_timestamp):
        start_timestamp = int(start_timestamp)
        end_timestamp = int(end_timestamp)
        try:
            redis_pool = await self.get_redis_pool()
            async with redis_pool.client() as conn:
                counters = await conn.zrangebyscore(
                    self.redis_key,
                    start_timestamp,
                    end_timestamp,
                )
        except aioredis.RedisError:
            raise ServerError("Failed to reach backend, how embarrassing")
        return [eval(c) for c in counters]

    def start_cleanup_thread(self):
        async def cleanup_loop():
            while True:
                # Calculate the cutoff timestamp
                cutoff = int(time.time()) - self.max_interval

                # Remove all items older than max interval from the Redis list
                try:
                    redis_pool = await self.get_redis_pool()
                    async with redis_pool.client() as conn:
                        await conn.zremrangebyscore(self.redis_key, 0, cutoff)
                except aioredis.RedisError:
                    raise ServerError("Failed to reach backend, how embarrassing")

                # Wait for one second before running the loop again
                # short period for the sake of testing
                await asyncio.sleep(self.cleanup_period)

        # Create a new thread and start the cleanup loop
        asyncio.create_task(cleanup_loop())
