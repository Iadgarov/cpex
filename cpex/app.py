import os
import time

import uvicorn
from fastapi import FastAPI, HTTPException
from fastapi.responses import JSONResponse
from pydantic import BaseModel

from cpex.exceptions import UserError
from cpex.keyword_counter import KeywordCounter

app = FastAPI()

KEYWORDS = ["checkpoint", "avanan", "email", "security"]
MAX_INTERVAL = os.environ.get(
    "MAX_INTERVAL", default=60
)  # default max interval of a minute
keyword_counter = KeywordCounter(KEYWORDS, MAX_INTERVAL)


class Input(BaseModel):
    sentence: str


@app.post("/api/v1/events")
async def events(input: Input):
    await keyword_counter.count_keywords(input.sentence)


@app.get("/api/v1/stats")
async def stats(interval: int):
    if interval not in range(1, MAX_INTERVAL):
        raise UserError(
            "Invalid interval. Must be integer > 0 and < {}".format(MAX_INTERVAL)
        )

    now = int(time.time())
    start_timestamp = now - interval
    end_timestamp = now
    counters = await keyword_counter.get_counts_by_timestamp_range(
        start_timestamp, end_timestamp
    )

    return {kw: sum([c[kw] for c in counters]) for kw in KEYWORDS}


@app.exception_handler(HTTPException)
async def http_exception_handler(request, exc):
    return JSONResponse(status_code=exc.status_code, content={"message": exc.detail})


if __name__ == "__main__":
    # Start the periodic cleanup of the counters queue in the background
    keyword_counter.start_cleanup_thread()

    # Start the FastAPI server
    uvicorn.run(app, host="0.0.0.0", port=8000)
