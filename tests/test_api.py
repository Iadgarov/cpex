from unittest.mock import patch

import pytest
from starlette.testclient import TestClient

from cpex.app import app


@pytest.fixture
def client():
    with TestClient(app) as client:
        yield client


@pytest.fixture
def mock_keyword_counter():
    with patch("cpex.app.keyword_counter", autospec=True) as mock:
        yield mock


def test_events(client, mock_keyword_counter):
    mock_keyword_counter.count_keywords.return_value = {
        "checkpoint": 1,
        "avanan": 0,
        "email": 2,
        "security": 0,
    }

    response = client.post(
        "/api/v1/events",
        json={"sentence": "This is a checkpoint email and another email."},
    )

    assert response.status_code == 200

    mock_keyword_counter.count_keywords.assert_called_once_with(
        "This is a checkpoint email and another email."
    )


def test_events_with_empty_sentence(client, mock_keyword_counter):
    mock_keyword_counter.count_keywords.return_value = {
        "checkpoint": 0,
        "avanan": 0,
        "email": 0,
        "security": 0,
    }

    response = client.post("/api/v1/events", json={"sentence": ""})

    assert response.status_code == 200

    mock_keyword_counter.count_keywords.assert_called_once_with("")


def test_events_with_missing_sentence(client):
    response = client.post("/api/v1/events")

    assert response.status_code == 422


def test_stats(client, mock_keyword_counter):
    mock_keyword_counter.get_counts_by_timestamp_range.return_value = [
        {"checkpoint": 1, "avanan": 0, "email": 2, "security": 0},
        {"checkpoint": 2, "avanan": 1, "email": 1, "security": 0},
    ]

    with patch("time.time", return_value=99999999):  # mock the time.time() call
        response = client.get("/api/v1/stats?interval=10")

    assert response.status_code == 200
    assert response.json() == {"checkpoint": 3, "avanan": 1, "email": 3, "security": 0}

    start_timestamp = 99999999 - 10
    mock_keyword_counter.get_counts_by_timestamp_range.assert_called_once_with(
        start_timestamp, 99999999
    )


def test_stats_with_bad_interval(client):
    for bad_interval in [None, -1, 99999, 0]:
        response = client.get(
            "/api/v1/stats{}".format(
                "?interval=" + str(bad_interval) if bad_interval else ""
            )
        )
        assert response.status_code == 422
