from unittest.mock import patch

import fakeredis
import pytest
from fakeredis import aioredis

from cpex.keyword_counter import KeywordCounter


@pytest.mark.asyncio
async def test_count_keywords():
    # Initialize a KeywordCounter object with a mock Redis client
    kc = KeywordCounter(["email", "checkpoint"], 3600)
    fr = aioredis.FakeRedis(server=fakeredis.FakeServer())
    kc.redis_pool = fr

    # Call count_keywords method with a sentence
    with patch("random.random", return_value=123), patch("time.time", return_value=5):
        await kc.count_keywords("email email checkpoint")
        start_timestamp, end_timestamp = 0, 100
        counters = await kc.get_counts_by_timestamp_range(
            start_timestamp, end_timestamp
        )
        assert counters == [{"email": 2, "checkpoint": 1, "random_seed": 123}]
